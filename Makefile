# Sogilis
# 4 avenue du Doyen Louis Weil
# 38000 Grenoble

IMG = tensorflow_docker
CONTAINER_IMAGES_DIR = /home/dev/training_dataset/images
CONTAINER_XMLS_DIR = /home/dev/training_dataset/annotations/xmls
CONTAINER_DATA_RECORD = /home/dev/data_record
CONTAINER_MODEL_DIR = /home/dev/training_model/model/

.PHONY  = all clean

all : run

build : begin docker_build end

run : docker_run

debug : docker_interract

docker_build :
	docker build -t $(IMG) .

docker_run :
ifeq ("$(shell docker images -q $(IMG))","")
	@echo "No $(IMG) image found. Please run <make build>"
else
	docker run -v $(shell pwd)/images:$(CONTAINER_IMAGES_DIR) -v $(shell pwd)/annotations:$(CONTAINER_XMLS_DIR) -v $(shell pwd)/data_record:$(CONTAINER_DATA_RECORD) -v $(shell pwd)/trained_model:$(CONTAINER_MODEL_DIR) $(IMG) bash -c '/home/dev/launch.sh'
endif

docker_output_record :
ifeq ("$(shell docker images -q $(IMG))","")
	@echo "No $(IMG) image found. Please run <make build>"
else
	docker run -v $(shell pwd)/images:$(CONTAINER_IMAGES_DIR) -v $(shell pwd)/annotations:$(CONTAINER_XMLS_DIR) -v $(shell pwd)/data_record:$(CONTAINER_DATA_RECORD) $(IMG) bash -c '/home/dev/export_record.sh'
endif

docker_tensorboard :
ifeq ("$(shell docker images -q $(IMG))","")
	@echo "No $(IMG) image found. Please run <make build>"
else
	@echo "\033[0;32mGo to the url http://localhost:6006 on your favorite web browser and see the magic happen\033[0m"
	docker run -v $(shell pwd)/trained_model:$(CONTAINER_MODEL_DIR) -p 6006:6006 $(IMG) bash -c 'tensorboard --logdir /home/dev/training_model/model/train'
endif
#docker_eval:
#ifeq ("$(shell docker images -q $(IMG))","")
#	@echo "No $(IMG) image found. Please run <make build>"
#else
#	docker run -v $(shell pwd)/trained_model:$(CONTAINER_MODEL_DIR) -v $(shell pwd)/data_record:$(CONTAINER_DATA_RECORD) $(IMG) bash -c '/home/dev/eval.sh'
#endif

docker_interract:
ifeq ("$(shell docker images -q $(IMG))","")
	@echo "No $(IMG) image found. Please run <make build>"
else
	docker run -v $(shell pwd)/images:$(CONTAINER_IMAGES_DIR) -v $(shell pwd)/annotations:$(CONTAINER_XMLS_DIR) -it $(IMG) bash
endif

clean :
	docker ps -a | awk '{ print $$1,$$2 }' | grep $(IMG) | awk '{print $$1 }' | xargs -I {} docker rm {}
	docker rmi $(IMG)

begin :
	@date +"Starting at +%FT%T%z"

end :
	@date +"Done at +%FT%T%z"

#!/bin/sh
python object_detection/eval.py \
    --logtostderr \
    --pipeline_config_path=training_model/model_ssd_mobilenet_v1.config \
    --checkpoint_dir=training_model/model/train \
    --eval_dir=training_model/model/eval


import os
import random

fl = os.listdir('/home/dev/training_dataset/annotations/xmls')
pref = '/home/dev/training_dataset/annotations/'
#fl = os.listdir('../images')
k = set([])

#Create label_map
cl_name = fl[0].split('_')[0]
with open('/home/dev/data_record/label_map.pbtxt', 'w') as f:
    f.write('item {\n')
    f.write('  id: 1\n')
    f.write('  name: \'' + cl_name + '\'\n')
    f.write('}\n')


for fn in fl:
    k.add(fn.split('.')[0])

with open(pref + 'list.txt', 'w') as f:
    for i in k:
        f.write(i + ' 1\n')

with open(pref + 'test.txt', 'w') as f:
    nb = int(len(k) * 0.25)
    for i in range(0,nb):
        c = random.choice(tuple(k))
        k.remove(c)
        f.write(c + ' 1\n')

with open(pref + 'trainval.txt', 'w') as f:
    for i in k:
        f.write(i + ' 1\n')

#!/bin/sh
python parse_data_names.py
python convert_dataset.py --label_map_path=data_record/label_map.pbtxt --data_dir=training_dataset/ --output_dir=data_record/
python object_detection/train.py \
    --logtostderr \
    --pipeline_config_path=training_model/model_ssd_mobilenet_v1.config \
    --train_dir=training_model/model/train

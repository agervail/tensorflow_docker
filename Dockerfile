FROM tensorflow/tensorflow:latest-py3

# My name is Corentin <corentin@sogilis.com>
MAINTAINER Corentin

RUN apt-get update \
    && apt-get install -y protobuf-compiler git python3-tk

RUN pip install lxml

RUN mkdir /home/dev

COPY build /home/dev

RUN cd /home/dev \
    && mkdir training_dataset training_dataset/annotations training_dataset/annotations/xmls training_dataset/data training_dataset/images \
    && mkdir training_model/train

RUN cd /home/dev \
    && protoc object_detection/protos/*.proto --python_out=.

ENV PYTHONPATH=$PYTHONPATH:.:slim

WORKDIR /home/dev
